# (?, 1, 10) -> (?, 1)
def pos_get_pred_output(data_embed):
    # include output layer
    n_layer = len(layer_dim)  # 3
    data_embed_dnn = tf.reshape(data_embed, [-1, k])  # (?, 10)
    cur_layer = data_embed_dnn
    # loop to create DNN struct
    for i in range(0, n_layer):
        # output layer, linear activation
        if i == n_layer - 1:
            cur_layer = tf.matmul(cur_layer, pos_weight_dict[i]) + pos_bias_dict[i]
        else:
            cur_layer = tf.nn.relu(tf.matmul(cur_layer, pos_weight_dict[i]) + pos_bias_dict[i])
            cur_layer = tf.nn.dropout(cur_layer, keep_prob)

    y_hat = cur_layer
    return y_hat

# pos 网络的 FC 层
pos_n_layer = len(layer_dim)  # [512, 256, 1] 层数 3
pos_in_dim = k  # nn 输入的维度 10
pos_weight_dict = {}
pos_bias_dict = {}

# loop to create pos vars 这是骨架
for i in range(0, n_layer):
    out_dim = layer_dim[i]
    pos_weight_dict[i] = tf.Variable(tf.random_normal(shape=[pos_in_dim, out_dim], stddev=np.sqrt(2.0/(pos_in_dim+out_dim))))
    pos_bias_dict[i] = tf.Variable(tf.constant(0.0, shape=[out_dim]))
    pos_in_dim = layer_dim[i]
