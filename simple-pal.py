def read(path):
    """TODO: Docstring for read.

    :path: TODO
    :returns: TODO

    """
    import csv
    with open(path) as f:
        f_csv = csv.reader(f)

        line = []
        for row in f_csv:
            line.extend(row)
        line = [float(i) for i in line]

        return line


if __name__ == "__main__":
    #  __import__('pudb').set_trace()
    line_pos = read('./simple-pal.pos.txt')
    line_pred = read('./simple-pal.pred.txt')

    print(len(line_pos))
    print(len(line_pred))
