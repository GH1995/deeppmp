# __import__('pudb').set_trace()
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics
from math import sqrt
from pyfm import pylibfm

file_list = ['../data/day_1.csv', '../data/day_2.csv', ]
# file_list = ['./test/day_1.csv', './test/day_2.csv', ]

l = []
for filename in file_list:
    df = pd.read_csv(filename, header=None)
    l.append(df)

train = pd.concat(l, axis=0, ignore_index=True)
# test = pd.read_csv('./data/day_4.csv')
val = pd.read_csv('../data/day_3.csv', header=None)
test = pd.read_csv('../data/day_4.csv', header=None)


def get_feature(data):
    return data[list(range(1, 561))]


def get_label(data):
    return data[list(range(1))]


x_train = get_feature(train)
y_train = get_label(train)
x_val = get_feature(val)
y_val = get_label(val)
x_test = get_feature(test)
y_test = get_label(test)

model = LinearRegression()
model.fit(x_train, y_train)

y_train_s = model.predict(x_train)
y_val_s = model.predict(x_val)
y_predict = model.predict(x_test)

print("train LogLoss: %s\n" % metrics.log_loss(y_true=y_train, y_pred=y_train_s))

print("val AUC: %s\n" % metrics.roc_auc_score(y_true=y_val, y_score=y_val_s))

print("test AUC: %s\n" % metrics.roc_auc_score(y_true=y_test,y_score=y_predict))
print("test LogLoss: %s\n" % metrics.log_loss(y_true=y_test, y_pred=y_predict))
print("test rmse: %s\n" % sqrt(metrics.mean_squared_error(y_true=y_test, y_pred=y_predict)))
